/*
 * HilosRtc.cpp
 * 
 * Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

// thread example
#include <iostream>       // std::cout
#include <thread>         // std::thread
#include "rtc.h" 
#include "myUsb.h"
#include "../SimuladorMotor/PID.h"
#include "../SimuladorMotor/sim_modulo.h"
#include <stdio.h>
//para escribir en un archivo
#include <iostream>
#include <fstream>
#include "serverSocket.h"
#include <string.h>
#include <stdlib.h>

using namespace std;
double velocidadDeseada = 100;
double actualSpeed = 0;
double nuevoTorque = 0;

void HiloModulo() 
{
    double kp = 0.5, kd = 0.2, ki = 0.01;
    PID pid = PID(kp, ki, kd);
    rtc rtcMio =rtc(32);
    //myUsb modulo = myUsb();
    sim_modulo modulo = sim_modulo(210, 0.03, 20);
    while(true){
        
        rtcMio.tick();
       
        actualSpeed = modulo.getSpeed();
        nuevoTorque = pid.getPID(velocidadDeseada,actualSpeed);
        modulo.setTorque(nuevoTorque);
        
    }

}
void HiloSocket(){
     serverSocket socket = serverSocket();
    char A[30];
    char *parametros;
    char parametros2[23];
    char *token;
    //~ double speedKiKpKd[4];
    while(true){
        //se queda clavado con esto
    parametros = socket.readMessage();
    //~ speedKiKpKd[0] = strtod(parametros, &token);
    //~ speedKiKpKd[1] = strtod(++token, &token);
    //~ speedKiKpKd[2] = strtod(++token, &token);
    //~ speedKiKpKd[3] = strtod(++token, &token);
    //~ velocidadDeseada = strtod(parametros, &token);
    //~ kp = strtod(++token, &token);
    //~ ki = strtod(++token, &token);
    //~ kd = strtod(++token, &token);
    free(parametros);
    sprintf (A, "%g,%g\n", actualSpeed, nuevoTorque);
    socket.sendMessage(A);
    }
    
    
}
int main() 
{
   
  std::thread first (HiloModulo);     // spawn new thread that calls foo()
  std::thread second (HiloSocket);     // spawn new thread that calls foo()

  std::cout << "main, foo and bar now execute concurrently...\n";

  // synchronize threads:
  first.join();                // pauses until first finishes
  //second.join();
  std::cout << "foo and bar completed.\n";

  return 0;
}


