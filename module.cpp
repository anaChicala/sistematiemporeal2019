/*
 * module.cpp
 * 
 * Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include "myUsb.h"

int main(int argc, char **argv)
{
	myUsb modulo = myUsb();
    
    modulo.setTorque(5);
    char* mensaje  =  "CHINWENWENCHA";
    modulo.sendMessage(mensaje);
    modulo.clearScreen();
     unsigned char speed = modulo.getSpeed();
    while (true){
       printf("%lf\n", modulo.getSpeed());
      fflush(stdout);
    }
    // printf(speed);
    //fflush(stdout);
	return 0;
}

