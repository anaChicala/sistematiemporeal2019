#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  GUI.py
#  
#  Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib
import time
import socket as skt
HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 8081        # The port used by the server
class Client ():
    def __init__(self, host, port):
        """ Create the socket, but do NOT connect yet. That way we
            can still change some parameters of the 
        """
        self.host = host
        self.port = port
        self.socket = skt.socket(skt.AF_INET, skt.SOCK_STREAM)    # Construir el zocalo
        self.show_status_cb = None
        self.show_data_cb = None
        self.data = '\n'
    def connect(self):
        """ Now really try to connect to the server.
            I really need to add an exception handler in case the server
            is not on-line.
        """
        self.socket.connect((HOST, PORT))
        self.socket.setblocking(False)
        self.fd = self.socket.fileno()             # Obtener el file descriptor
        # ~ self.show_status("Connected to " + self.host)
        GLib.io_add_watch(self.fd, GLib.IO_IN, self.process_input)   # Instalar la funcion en caso de datos recibidos
        
    def process_input(self, skt, cond):
        """ This function is called asynchronously when data is received.
        """
        msg = self.socket.recv(100)                # Recibir el mensage de la red
        self.show_data(msg)                        # Enviar los datos a MainWindow
        return True                                # Queremos quedar activos
        
    def send(self, msg):
        self.socket.send(msg)
    
    def show_data(self, msg):
        """ Show received messages, either on the terminal,
            or else by calling the callback installed by set_data_show().
        """
        # ~ if self.show_data_cb == None:
        msg = msg.decode('utf-8')
        msg = msg.rstrip('\n')
        msg = msg.split(",")
        print ("Data: ", msg)
        print ("Data: ", msg[0])
        print ("Data: ", msg[1])
        # ~ else:
        # ~ self.show_data_cb(msg.decode('utf-8'))
    
    def set_data_show(self, show_data_cb):
        self.show_data_cb = show_data_cb
        
class MainWindow(Gtk.Window):
    def __init__(self):
        self.builder = Gtk.Builder.new_from_file("interfaz.glade")
        self.ventana = self.builder.get_object("ventana")
        self.ventana.show()

    def run(self):
        Gtk.main()
        
    def show_data(self, msg):
        self.velocidadActual = self.builder.get_object("velocidadActual")
        self.torqueActual = self.builder.get_object("torqueActual")
        msg = msg.rstrip('\n')
        msg = msg.split(",")
        self.velocidadActual.set_text(msg[0])
        self.torqueActual.set_text(msg[1])
    def set_client(self, client):
        """ The client is the communication system which wants to show
            things here, and get strings to transmit from the GtkEntry
        """
        self.client = client
        # We connect callbacks to get news from the client
        # ~ client.set_status_show(self.show_status)
        client.set_data_show(self.show_data)
        
    def destroy(self, window, data):
        """ Called when the MainWindow is closed - this stops the
            GTK+ main loop, and terminates the program
        """
        gtk.main_quit()
        
def main(args):
    main = MainWindow()                # Prepare the main window,
    client = Client(HOST, PORT)        # and the communications package
    main.set_client(client)            # Announce the client to the main window
    client.connect()                   # and try to do the real connection
    main.run()
    
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
