/*
 * test_sim_modulo.cpp
 * 
 * Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include "sim_modulo.h"
#include "PID.h"
#include "myUsb.h"
#include <unistd.h>

int main(int argc, char **argv)
{
    double kp = 0.5, kd = 0, ki = 0.01;
	//~ sim_modulo modulo = sim_modulo(210, 0.03, 20);
    myUsb modulo = myUsb();
    PID pid = PID(kp, ki, kd);
    double velocidadDeseada = 20;
    int nuevoTorque = 0; // (solamente entero y tiene que tomar valores entre 0 y 255)
    double actualSpeed = 0;
   while(true){
        usleep(32000);
        actualSpeed = modulo.getSpeed();
        printf("%g\n",actualSpeed);
        nuevoTorque = pid.getPID(velocidadDeseada,actualSpeed);
        modulo.setTorque(nuevoTorque);
   }
	//~ return 0;
}

