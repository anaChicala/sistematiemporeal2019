#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  sim_modulo.py
#  
#  Copyright 2019 Unknown <root@hp425>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


import pylab as plt
import numpy as np
import scipy
from math import exp


class Modulo():
    def __init__(self, v_max, v_sens, inercia, init_torque = 0):
        self.v_max, self.v_sens = v_max, v_sens
        self.inercia = inercia
        self.act_speed = self.static(init_torque)
        
        
    def static(self, tque):
        return self.v_max * (1 - exp(-tque*self.v_sens))
        
        
    def dynamic(self, tque):
        diff =  self.static(tque) - self.act_speed
        self.act_speed += diff / self.inercia
        return self.act_speed
        
        
    def set_torque(self, tque):
        return self.dynamic(tque)
        
        
    def get_speed(self):
        return self.act_speed
        
        
        
def test_static():
    m = Modulo(210, 0.03, 20, 20)
    
    ts = []
    v = []
    for t in range(256):
        ts.append(t)  
        v.append(m.static(t))
        
    plt.plot(ts, v)
    plt.grid(True)
    plt.xlabel("Torque")
    plt.ylabel("Velocidad")
    plt.show()
    
    
def test_dynamic():
    m = Modulo(210, 0.03, 20, 20)
    
    v = []
    # ~ for t in range(30):
        # ~ v.append(m.dynamic(20))
        
    for t in range(150):
        v.append(m.dynamic(40))
        
    # ~ for t in range(200):
        # ~ v.append(m.dynamic(255))
        
        
    plt.plot(v)
    plt.grid(True)
    plt.xlabel("Tiempo")
    plt.ylabel("Velocidad")
    plt.show()


def main(args):
    test_static()
    # ~ test_dynamic()
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
