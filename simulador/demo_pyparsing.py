#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  demo_pyparsing.py
#  
#  Copyright 2018 Unknown <root@hp425>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from pyparsing import Word, Literal, Combine
from pyparsing import Optional, MatchFirst
from pyparsing import alphas, nums, oneOf
 
TEST = """
#define __SFR_OFFSET 1

    .text
    
main:   adc     r24
        add     r15
"""

def bnf():
    hashtag = Literal("#")
    dot = Literal(".")
    colon = Literal(":")
    offset = Literal("__SFR_OFFSET")

    identifier = Word( alphas, alphas+nums+"_" )
    integer = Word( nums )
    hash_direc = Combine(hashtag + identifier) + offset + integer
    label = identifier + colon
    opcode = oneOf("adc add")
    
    dot_direc = dot + identifier
    
    directive = hash_direc | dot_direc
    source = Optional(label) + Optional(opcode)
    
    line = directive | source 
    
    return line


def main(args):
    for line in TEST.split("\n"):
        print("Parsing: ", line)
        res = bnf().parseString(line)
        print(res)
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
