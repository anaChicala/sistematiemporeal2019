/*
 * sim_modulo.cpp
 * 
 * Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include "sim_modulo.h"
#include <math.h>
       
sim_modulo::sim_modulo(double v_max, double v_sens, double inercia, double init_torque){
    this->v_max = v_max;
    this->v_sens = v_sens;
    this->inercia = inercia;
    this->init_torque = init_torque;
    this->act_speed = estatico(this->init_torque);
}

double
sim_modulo::estatico(double tque){
    return this->v_max * (1 - exp(-tque * this->v_sens));
}

void
sim_modulo::dinamico(double tque){
    double diff = estatico(tque) - this->act_speed;
    this->act_speed += (double)diff / this->inercia;
    
}

void
sim_modulo::setTorque(double tque){
    dinamico(tque);
}

double 
sim_modulo::getSpeed(){
    return this->act_speed;
}
