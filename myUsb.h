#define MAX_STR 255
#include "hidapi.h"
typedef unsigned char byte;
class myUsb {
    public:
		myUsb();
		void setTorque (byte torque);
        double  getSpeed ();
        void sendMessage (char* mensaje);
        void clearScreen ();
	private: 	
		int res;
        byte buf[33];
        wchar_t wstr[MAX_STR];
        hid_device *handle;
        int i;
};
