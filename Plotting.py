#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Reader.py
#  
#  Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import csv
import pylab as plt 
class Plotting():
        
    def writter (self, x, y, name):
        with open(name +".csv", "w") as out:
            for i in x:
                print(str(x[i])+ "," + str(y[i]), file = out)
        

    def readerAndPlot(self, name1, name2):
        x1 = []
        x2 = []

        fig, axs = plt.subplots(2, 2)
        with open(name1, "r") as inp:
            rdr = csv.reader(inp)
            for line in rdr:
                x1.append(float(line[0]))
                x2.append(float(line[1]))
        axs[0, 0].grid(True)
        axs[0, 0].plot(x1)
        axs[0, 0].set_title('Velocidad vs Tiempo con KD = 0')
        axs[0, 0].set_xlabel('Tiempo')
        axs[0, 0].set_ylabel('Velocidad')
        
        axs[0, 1].grid(True)
        axs[0, 1].plot(x2)
        axs[0, 1].set_xlabel('Tiempo')
        axs[0, 1].set_ylabel('Torque')
        axs[0, 1].set_title('Torque vs Tiempo con KD = 0')
        x1 = []
        x2 = []
        with open(name2, "r") as inp:
            rdr = csv.reader(inp)
            for line in rdr:
                x1.append(float(line[0]))
                x2.append(float(line[1]))
        axs[1, 0].grid(True)
        axs[1, 0].plot(x1)
        axs[1, 0].set_ylabel('Velocidad')
        axs[1, 0].set_xlabel('Tiempo')
        axs[1, 0].set_title('Velocidad vs Tiempo con KD = 0.1')

        axs[1, 1].grid(True)
        axs[1, 1].plot(x2)
        axs[1, 1].set_ylabel('Torque')
        axs[1, 1].set_xlabel('Tiempo')
        axs[1, 1].set_title('Torque vs Tiempo con KD = 0.1')
        
        fig.tight_layout()
        plt.grid(True)
        plt.show()
        
def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
