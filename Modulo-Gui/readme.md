# STR 2019

## Archivos importantes:
### [GUI.py](https://bitbucket.org/anaChicala/sistematiemporeal2019/src/master/Modulo-Gui/GUI.py)

* Interfaz grafica que permite al usuario que interactue con el modulo.
Consiste en:
- Un grafico que muestra al usuario la variacion del torque y la velocidad en tiempo real.
- Un slider para controlar la velocidad deseada
- Coeficientes de error a utilizar a la hora de calcular el torque necesario para llegar a la velocidad deseada (Kp, Ki, Kd)


### [interfazGrid2.glade](https://bitbucket.org/anaChicala/sistematiemporeal2019/src/master/Modulo-Gui/interfazGrid2.glade) 
* interfaz que instancia GUI.py.

### [Makefile](https://bitbucket.org/anaChicala/sistematiemporeal2019/src/master/Modulo-Gui/Makefile) 
* archivo a ejecutar cada vez que hay una modificacion de algun cpp.
* Para ejecutar con el simulador hay que comentarizar las lineas 17,18,20,21 y descomentarizar las lineas 9,10,12,13.
* Para utilizat con el modulo real, hay que descomentarizar las lineas 17,18,20,21 y comentarizar las lineas 9,10,12,13

### [HilosRtc.cpp](https://bitbucket.org/anaChicala/sistematiemporeal2019/src/master/Modulo-Gui/HilosRtc.cpp) 
 * programa principal que crea el servidor y arranca el modulo.
* Para ejecutar con el simulador hay que comentarizar la linea 54 y descomentarizar la linea 56.
* Para ejecutar con el modulo real hay que descomentarizar la linea 54 y comentarizar la linea 56

### [myUsb.h](https://bitbucket.org/anaChicala/sistematiemporeal2019/src/master/Modulo-Gui/myUsb.h) 
* libreria para comunicarse con el modulo.

### [rtc.h](https://bitbucket.org/anaChicala/sistematiemporeal2019/src/master/Modulo-Gui/rtc.h) 
* libreria para usar el clock del SO.

### [serverSocket.h](https://bitbucket.org/anaChicala/sistematiemporeal2019/src/master/Modulo-Gui/serverSocket.h)
* libreria para crear el servidor.

### [TCP.py](https://bitbucket.org/anaChicala/sistematiemporeal2019/src/master/Modulo-Gui/TCP.py)
* Se utiliza para pasar datos entra la interfaz grafica y el cliente que implementa el programa Client.py

### [Client.py](https://bitbucket.org/anaChicala/sistematiemporeal2019/src/master/Modulo-Gui/Client.py)
* Impementa un cliente que envia datos (kp, ki, kd, y velocidad) al servidor

## Como Ejecutar
1. Realizar las modificaciones segun lo que dice el apartado  [makefile](#makefile) y el  apartado  [HilosRtc.cpp](#HilosRtc.cpp )
2. En la consola ir hasta el directorio `Modulo-Gui`
3. Ejecutar el comando `make`
4. Ejecutar el comando `sudo ./RtcHilos`
5. En otra terminal ir al directorio `Modulo-Gui`
6. Ejecutar el comando `python3 GUI.py`

##Protocolo de comunicacion

Para lograr la comunicacion entre el cliente y el modulo se utilizara

Desde el cliente se envian 4 valores separados por coma: Velocidad deseada, Kp, Ki, Kd (siendo estos 3  ultimos los coeficientes de error a utilizar a la hora de calcular el torque necesario para llegar a la velocidad deseada)
Luego con estos 4 valores se calculara el torque necesario para llegar a la velocidad deseada y este torque es el que se le enviara al modulo.
La informacion es enviada en bytes

Del modulo al cliente se envian 2 valores separados por coma : la velocidad y el torque actual.
La informacion es enviada en bytes