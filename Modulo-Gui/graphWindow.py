#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  graphWindow.py
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GooCanvas', '2.0')
from gi.repository import Gtk, GooCanvas,GLib
import csv
class GraphWindow(GooCanvas.CanvasGroup):
    def __init__(self, pointsQuantity, color, parent, x = 0, y = 0):
        super(GraphWindow, self).__init__(parent = parent, x = x, y = y)
        self.cantPuntos = pointsQuantity
        self.x = []
        self.y = []
        for i in range (self.cantPuntos):
            self.x.append(i)
            self.y.append(0)
        
        self.point = GooCanvas.CanvasPoints.new(self.cantPuntos)
        self.line1 = GooCanvas.CanvasPolyline(
            parent = self,
            points = self.point,
            close_path = False,
            stroke_color_rgba = color,
            line_width = 1
            )    
        self.timer = 0

        
    def addPoint(self, valorX, valorY):

        for i in range(self.cantPuntos):
            self.point.set_point(i, self.x[i], 300 - self.y[i])
            
        self.line1.set_property('points', self.point)
        self.y.pop(0)
        self.y.append(valorY)

        
    def probarGrafico(self):
        self.timer = 0
        GLib.timeout_add(1000, self.probarPaso)
    
    def probarPaso(self):
        self.addPoint(self.timer, self.timer ** 2)
        self.timer += 1
        return self.timer != -10
     
    def graficar(self,point):
        self.addPoint(self.timer, point)
    def run(self):
        Gtk.main()
       
       
def main(args):
    mainwdw = GraphWindow(20)
    mainwdw.probarGrafico()
    mainwdw.run()
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
