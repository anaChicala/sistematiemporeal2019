#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  TCP.py
#  
#  Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from Client import Client
# ~ from GUI import MainWindow as GUI
HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 8081        # The port used by the server
class TCP():
    def __init__(self, kp, ki, kd, speed, handler = None):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.speed = speed
        self.client = Client(HOST, PORT, self.getMessage)        # and the communications package
        self.set_client(self.client)            # Announce the client to the main window
        self.client.connect()                   # and try to do the real connection
        self.handler = handler
    def setKP(self, kp):
        self.kp = kp
    
    def setKI(self, ki):
        self.ki = ki
    
    def setKD(self, kd):
        self.kd = kd
        
    def setSpeed(self, speed):
        self.speed = speed;
            
    def sendMessage(self):
        pids = "{:g},{:},{:},{:}".format(self.speed, self.kp, self.ki, self.kd)
        self.client.send(pids)
        
    def set_client(self, client):
        """ The client is the communication system which wants to show
            things here, and get strings to transmit from the GtkEntry
        """
        self.client = client
        
    
    def getMessage(self, msg):
        if self.handler != None:
            self.handler(msg)
        # ~ GUI.show_data(msg);
        
    
def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
