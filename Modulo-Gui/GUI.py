#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GooCanvas', '2.0')
from gi.repository import Gtk, GooCanvas,GLib
import time
from TCP import TCP
from graphWindow import GraphWindow
import sys

class Wewewe():
    def __init__(self, pointsQuantity):
        
        self.tcp = TCP(0, 0, 0, 0, self.show_data)
        self.builder = Gtk.Builder.new_from_file("interfazGrid2.glade")
        
        
        self.ventana = self.builder.get_object("ventana")
        self.ventana.connect('destroy', self.destroy)
        
        self.scale = self.builder.get_object("setVelocidad")
        self.scale.connect('button-release-event', self.valueChanged)
        
        self.entryKP = self.builder.get_object("kp")
        self.entryKP.connect('activate', self.kp_activate_cb)
        
        self.entryKI = self.builder.get_object("ki")
        self.entryKI.connect('activate', self.ki_activate_cb)
        
        self.entryKD = self.builder.get_object("kd")
        self.entryKD.connect('activate', self.kd_activate_cb)
       
        cv = GooCanvas.Canvas()
        # And ask for access to the root layer
        layer0 = cv.get_root_item()
        # Create a new layer, offset by x=130, and y=50
        self.layer1 = GooCanvas.CanvasGroup(
                    parent = layer0,
                    x = 200, y = 100)
        self.drawAxes(self.layer1)
        for i in range (94):
            self.drawXUnits(self.layer1, i+1)
        
        for i in range (29):
            self.drawYUnits(self.layer1,i+1)
            
        self.layer2 = GooCanvas.CanvasGroup(
                parent = layer0,
                x = 950, y = -30)
                
        self.drawLabels(self.layer2,  0x0000ffff, 0xff0fe0ff)
       
        self.cantPuntos = pointsQuantity
        self.ejemplo = GraphWindow(self.cantPuntos, 0x0000ffff, parent = self.layer1)
        self.ejemplo2 = GraphWindow(self.cantPuntos, 0xff0fe0ff, parent = self.layer1)
        
        scrolledWindow =  self.builder.get_object("scrolledWindow")
        scrolledWindow.add(cv)
        
        # ~ draws grid
        for i in range (6):
            self.drawGrid(self.layer1, "Black", pointsQuantity, (i+1)*50, "x")
        for i in range (19):
            self.drawGrid(self.layer1, "Black", 300, i*50, "y")
            
        cv.set_bounds(0,0,1300,1100)
        cv.show()
      

    def run(self):
        self.ventana.show_all()
        Gtk.main()
        
    def show_data(self, msg):
        self.velocidadActual = self.builder.get_object("velocidadActual")
        self.torqueActual = self.builder.get_object("torqueActual")
        if len(msg) != 2 :
            # ~ print("error cant de parametros no es igual a 2")
            print(len(msg), msg)
            
        else:
            self.velocidadActual.set_text(msg[0])
            self.torqueActual.set_text(msg[1])
            try:
                # ~ print(msg)
                self.drawPoints(self.ejemplo, float(msg[0]))
                self.drawPoints(self.ejemplo2, float(msg[1]))
            except:
                # ~ print (msg)
                # ~ print (type(msg[0]), type(msg[1]))
                pass
                # ~ print("hola")
    def destroy(self, window):
        """ Called when the MainWindow is closed - this stops the
            GTK+ main loop, and terminates the program
        """
        Gtk.main_quit()
    def valueChanged(self, w, evt):
        value = w.get_value()
        self.tcp.setSpeed(value)
        # ~ print(value)
        self.tcp.sendMessage()
        
    def kp_activate_cb(self, w):

        value = w.get_text()
        self.tcp.setKP(value)
        self.tcp.sendMessage()
        
    def ki_activate_cb(self, w):
        value = w.get_text()
        self.tcp.setKI(value)
        self.tcp.sendMessage()
        
    def kd_activate_cb(self, w):
        value = w.get_text()
        self.tcp.setKD(value)
        self.tcp.sendMessage()
        
    def on_scrolledWindow_realize(self, w):
        pass
        
    def drawPoints(self, ejemplo, point):
        
        
        # ~ self.ejemplo2 = GraphWindow(20, 0xff00ff20, parent = layer)
        # ~ ejemplo.probarGrafico()
        ejemplo.graficar(point)
        # ~ self.ejemplo2.probarGrafico()

    def drawAxes(self, layer):
        # ~ draw y axis
        self.point = GooCanvas.CanvasPoints.new(2)
        self.point.set_point(0, 0, 0)
        self.point.set_point(1, 0, 300)
        self.line = GooCanvas.CanvasPolyline(
            parent = layer,
            x = 0, y = 0,
            points = self.point,
            close_path = False,
            stroke_color_rgba = 0x000000ff,
            line_width = 2,
            start_arrow = True
            )    
        #Draw a text at the y axis' origin
        text = GooCanvas.CanvasText(
                    parent = layer,
                    x = -60, y = 200,
                    text = "Unidad",
                    font = "Arial 20",
                    fill_color_rgba = 0x4e011ff)    
                    
        text.rotate(-90,-60,200)
        # ~ draw x axis
        self.point = GooCanvas.CanvasPoints.new(2)
        self.point.set_point(0, 0, 300)
        self.point.set_point(1, 950, 300)
        self.line = GooCanvas.CanvasPolyline(
            parent = layer,
            x = 0, y = 0,
            points = self.point,
            close_path = False,
            stroke_color_rgba = 0x000000ff,
            line_width = 2,
            end_arrow = True
            )    
            
        #Draw a text at the x axis' origin
        text = GooCanvas.CanvasText(
                    parent = layer,
                    x = 400, y = 315,
                    text = "Tiempo",
                    font = "Arial 20",
                    fill_color_rgba = 0xef0000ff)    
        
            
    def drawXUnits(self, layer, i):
        # ~ draw x axis units
        self.point = GooCanvas.CanvasPoints.new(2)
        self.point.set_point(0, i*10, 295)
        self.point.set_point(1, i*10, 305)
        self.width = 1
        
          
        #Draw a text at the x axis solo dibujo de 50 en 50 
        if i*10 % 50 == 0:
            self.width = 3
            text = GooCanvas.CanvasText(
                        parent = layer,
                        x = (i*10 + (i*10 -10))/2 , y = 310,
                        text = i*10,
                        font = "Serif 5",
                        fill_color_rgba = 0x000000ff)    
        
        self.line = GooCanvas.CanvasPolyline(
            parent = layer,
            x = 0, y = 0,
            points = self.point,
            close_path = False,
            stroke_color_rgba = 0x000000ff,
            line_width = self.width
            )  
                    
    def drawYUnits(self, layer, i):
        # ~ draw y axis units
        self.point = GooCanvas.CanvasPoints.new(2)
        self.point.set_point(0, -5, i*10)
        self.point.set_point(1, 5, i*10)
        self.width = 1
        if i*10 % 50 == 0:
            self.width = 3
            
        self.line = GooCanvas.CanvasPolyline(
            parent = layer,
            x = 0, y = 0,
            points = self.point,
            close_path = False,
            stroke_color_rgba = 0x000000ff,
            line_width = self.width
            )    
            
        #Draw a text at the y axis
        if (i * 10) % 50 == 0:
            text = GooCanvas.CanvasText(
                        parent = layer,
                        x = -20, y = (i*10 + (i*10 -10))/2 + 0.7,
                        text = 300 - i*10 ,
                        font = "Serif 5",
                        fill_color_rgba = 0x000000ff)    
    
    def drawLabels(self, layer, speedColor, torqueColor):
        # ~ rectangle
        rect = GooCanvas.CanvasRect(
                    parent = layer,
                    x = -40, y = 50,
                    width = 200, height = 75,
                    line_width = 1.0,
                    stroke_color = "black",
                    fill_color = "grey")
        # ~ Velocidad label                    
        text = GooCanvas.CanvasText(
                        parent = layer,
                        x = -30, y = 60,
                        text = "Velocidad" ,
                        font = "Arial 20",
                        fill_color_rgba = 0x000000ff)    
                        
        self.point = GooCanvas.CanvasPoints.new(2)
        self.point.set_point(0, 120, 77)
        self.point.set_point(1, 150, 77)
       

        self.line = GooCanvas.CanvasPolyline(
            parent = layer,
            points = self.point,
            close_path = False,
            stroke_color_rgba = speedColor,
            line_width = 1
            )
        self.point = GooCanvas.CanvasPoints.new(2)
        self.point.set_point(0, 120, 102)
        self.point.set_point(1, 150, 102)  
        self.line = GooCanvas.CanvasPolyline(
            parent = layer,
            points = self.point,
            close_path = False,
            stroke_color_rgba = torqueColor,
            line_width = 1
            )
        # ~ torque label
        text = GooCanvas.CanvasText(
                        parent = layer,
                        x = -5, y = 85,
                        text = "Torque" ,
                        font = "Arial 20",
                        fill_color_rgba = 0x000000ff)    
    def drawGrid(self, layer, color, size, i, axis):
         # ~ draw y axis units
        self.point = GooCanvas.CanvasPoints.new(2)
        if axis == "x":
            self.point.set_point(0, 0, i)
            self.point.set_point(1, size, i)
        if axis == "y":
            self.point.set_point(0, i, 0)
            self.point.set_point(1, i, size)
            
        self.width = 0.5
        self.line = GooCanvas.CanvasPolyline(
                parent = layer,
                x = 0, y = 0,
                points = self.point,
                close_path = False,
                stroke_color = color,
                line_width = self.width
                )    

def main(args):
    wewewe = Wewewe(950)                # Prepare the main window,
    wewewe.run()
    
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
