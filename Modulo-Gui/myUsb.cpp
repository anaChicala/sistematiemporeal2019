/*
 * Module.cpp
 * 
 * Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "hidapi.h"
#include "myUsb.h"
#include <string.h>
myUsb::myUsb (){
    // Initialize the hidapi library
	res = hid_init();

	// Open the device using the VID, PID,
	// and optionally the Serial number.
	handle = hid_open(0x04d8, 0xf83f, NULL);
}
void
myUsb::setTorque(byte torque){
    buf[0] = 1;
	buf[1] = torque;
	res = hid_write(handle, buf, 2);
    res = hid_exit();
}
double 
myUsb::getSpeed(){

    buf[0] = 3;
    res = hid_write(handle, buf, 2);
    res = hid_read(handle,buf,2);
    res = hid_exit();
    byte speed = buf[1];
    //~ double x = 1/15.258789;
    //~ double t = 60/x;
    //~ double cp =t*speed;
    //~ double rpm =cp/40;
    return speed;
}
void 
myUsb::sendMessage(char* mensaje ){
    for (i = 0; i <= 32; i++){
        buf[i] = ' ';
    }
    printf("Mensaje %d" , strlen(mensaje));
    for (i = 0; i < strlen(mensaje); i++){
            buf[i+1] = mensaje[i];
    }

    buf[0] = 2;
    res = hid_write(handle, buf, 33);
    res = hid_exit ();
}
void
myUsb::clearScreen(){
    for (i = 0; i <= 32; i++){
        buf[i] = ' ' ;
    }
    buf[1] = 0;
    buf[0] = 2;
    res = hid_write(handle, buf, 33);
    res = hid_exit ();
}

