/*
 * PID.cpp
 * 
 * Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include "PID.h"
PID::PID(){
}

int 
PID::getPID(double velocidadDeseada, double actualSpeed){
        int nuevoTorque;
        this->actualError = velocidadDeseada - actualSpeed;
        this->integralError = this->integralError + this->actualError;
        this->P = this->actualError * this->kp;
        this->I = this->ki * this->integralError;
        this->D = (this->actualError - this->previusError) * this->kd;
        nuevoTorque = (int) this->P + this->I + this->D;
        if(nuevoTorque < 0) 
            nuevoTorque = 0;
        if(nuevoTorque > 255 )
            nuevoTorque = 255;
        return nuevoTorque;
}
void 
PID::setKpKiKd(double kp, double ki, double kd){
    this->ki = ki;
    this->kp = kp;
    this->kd = kd;
}
