#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Client.py
#  
#  Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
# ~ from TCP import TCP
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib
import socket as skt

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 8081        # The port used by the server
class Client ():
    def __init__(self, host, port, handler = None):
        """ Create the socket, but do NOT connect yet. That way we
            can still change some parameters of the 
        """
        self.host = host
        self.port = port
        self.socket = skt.socket(skt.AF_INET, skt.SOCK_STREAM)    # Construir el zocalo
        self.show_status_cb = None
        self.show_data_cb = None
        self.data = '\n'
        self.handler = handler
    def connect(self):
        """ Now really try to connect to the server.
            I really need to add an exception handler in case the server
            is not on-line.
        """
        self.socket.connect((HOST, PORT))
        self.socket.setblocking(False)
        self.fd = self.socket.fileno()             # Obtener el file descriptor
        GLib.io_add_watch(self.fd, GLib.IO_IN, self.process_input)   # Instalar la funcion en caso de datos recibidos
        
    def process_input(self, skt, cond):
        """ This function is called asynchronously when data is received.
        """
        msg = self.socket.recv(100)                # Recibir el mensage de la red
        self.show_data(msg)                        # Enviar los datos a MainWindow
        return True                                # Queremos quedar activos
        
    def send(self, msg):

        self.socket.send(bytes(msg,'utf8'))
    
    def show_data(self, msg):
        """ Show received messages, either on the terminal,
            or else by calling the callback installed by set_data_show().
        """
                
        msg = msg.decode('utf-8')
        msg = msg.rstrip('\n')
        msg = msg.split(",")
        if self.handler != None:
            self.handler(msg)
    
    def set_data_show(self, show_data_cb):
        self.show_data_cb = show_data_cb
        
def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
