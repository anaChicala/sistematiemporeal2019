/*
 * HilosRtc.cpp
 * 
*/

#include <iostream>       
#include <thread>         
#include "rtc.h" 
#include "myUsb.h"
#include "PID.h"
#include "sim_modulo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "serverSocket.h"
#include <string.h>
#include <stdlib.h>

using namespace std;

struct KpKiKd{
    double kp;
    double ki;
    double kd;
};

double velocidadDeseada = 50;
double actualSpeed = 0;
double nuevoTorque = 0;
serverSocket skt;
struct KpKiKd kpkikd = {0,0,0};
PID pid = PID();
//~ Comentarizar si se usa el simulador
//~ myUsb modulo = myUsb();
//~ Comentarizar si se utiliza el modulo real
sim_modulo modulo = sim_modulo(210, 0.03, 20);
rtc rtcMio =rtc(32);
double kp = 0, ki = 0, kd = 0;
void HiloModulo(double *speed, double *torque, struct KpKiKd *kpkikd, double *velocidadDeseada, double *kp, double *ki, double *kd) 
{         
    char A[1];
    
    while(true){     
        rtcMio.tick();       
        pid.setKpKiKd(kpkikd->kp,kpkikd->ki,kpkikd->kd);
        *speed = modulo.getSpeed();
        *torque = pid.getPID(*velocidadDeseada,*speed);
        modulo.setTorque(*torque);
        sprintf (A, "%g,%g\n", *speed, *torque);
        skt.sendMessage(A);
    }

}
void HiloSocket(double *speed, double *torque, struct KpKiKd *kpkikd, double *velocidadDeseada, double *kp, double *ki, double *kd){
    
    char A[30];
    char *parametros;
    char parametros2[23];
    char *token;
    while(true){
        parametros = skt.readMessage();
        *velocidadDeseada = strtod(parametros, &token);
        kpkikd->kp = strtod(++token, &token);
        kpkikd->ki  = strtod(++token, &token);
        kpkikd->kd= strtod(++token, &token);
        free(parametros);
        printf(" speed: %g", *velocidadDeseada);
        printf(" kp: %g",kpkikd->kp);
        printf(" ki: %g",kpkikd->ki);
        printf(" kd: %g\n",kpkikd->kd);
    }
    
    
}
int main() 
{
   
  std::thread first (HiloModulo, &actualSpeed, &nuevoTorque, &kpkikd, &velocidadDeseada, &kp, &ki, &kd);     // spawn new thread that calls HiloModulo()
  std::thread second (HiloSocket, &actualSpeed, &nuevoTorque, &kpkikd, &velocidadDeseada, &kp, &ki, &kd);     // spawn new thread that calls HiloSocket()

  std::cout << "main, foo and bar now execute concurrently...\n";

  // synchronize threads:
  first.join();                // pauses until first finishes
  second.join();
  return 0;
}


