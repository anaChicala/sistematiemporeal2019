#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  clienteSocket.py
#  
#  Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  




import socket

class clientSocket():
    HOST = '127.0.0.1'  # The server's hostname or IP address
    PORT = 8080        # The port used by the server
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((HOST, PORT)) 
    
    def sendMessage(self, message):
        self.s.send(bytes(message, encoding = "iso-8859-1"))
        
    def getMessage(self):
        return self.s.recv(1024)
        
        
