#sim_cflags = ../SimuladorMotor/sim_modulo.h
#sim_libs= ../SimuladorMotor/sim_modulo.o
sim_cflags = ../SimuladorMotor/PID.h
sim_libs= ../SimuladorMotor/PID.o
all: module start RtcHilos socketServer.o 
module: module.cpp myUsb.o
	g++ -g -O0 -o module module.cpp myUsb.o  `pkg-config --libs --cflags hidapi-hidraw` 
RtcHilos: HilosRtc.cpp rtc.o myUsb.o  $(sim_libs) serverSocket.o
	g++ -o RtcHilos -pthread HilosRtc.cpp rtc.o myUsb.o $(sim_libs) serverSocket.o `pkg-config --libs --cflags hidapi-hidraw` 
start: start.cpp rtc.o
	g++ -o start start.cpp rtc.o
rtc.o: rtc.cpp rtc.h
	gcc -c rtc.cpp
myUsb.o: myUsb.cpp myUsb.h
	g++ -g -O0 -c myUsb.cpp `pkg-config --libs --cflags hidapi-hidraw`
socketServer.o: serverSocket.cpp serverSocket.h
	gcc -c serverSocket.cpp
#socketServer: socket.cpp
#	gcc socket.cpp -o socketServer
#socketClient: clienteSocket.cpp
#	gcc clienteSocket.cpp -o socketClient
